# -*- coding: utf-8 -*-
# Copyright (c) 2021 Wageningen Environmental Research
# Allard de Wit (allard.dewit@wur.nl), April 2021
import sys, os
import uuid
from pathlib import Path
from tqdm import tqdm

import sqlalchemy as sa
import pandas as pd
import geopandas as gpd
import numpy as np
import yaml
import duckdb

from .util import prepare_db, make_path_absolute, get_latlon_from_RD


class CSVLoadingError(Exception):
    """Exception for raising errors related to CSV reading
    """

    def __init__(self, error, trace_back):
        self.error = error
        self.traceback = trace_back
        super().__init__()


def load_parcel_info(grompy_yaml, gpkg_fname, counts_file_10m, counts_file_20m, counts_file_25m, shape_file, layer_name):
    """Loads the parcel info from the shapefile into a consistent geopackage

    :param gpkg_fname: File name where to write results as geopackage
    :param counts_file_10m: CSV file from which pixel counts should be read for the 10m bands
    :param counts_file_20m: CSV file from which pixel counts should be read for the 20m bands
    :param counts_file_25m: CSV file from which pixel counts should be read for the 25m bands
    :param shape_file: shapefile which should be read for parcel info.
    :param layer_name: name of the geopackage layer to write parcel info into

    :return:
    """
    shp_fname = make_path_absolute(grompy_yaml, Path(shape_file))
    df = gpd.read_file(shp_fname)
    df["fieldid"] = df.fieldid.astype(np.int32)
    df = df.set_index("fieldid")

    # Check for S2Tiles/S2_Tiles column
    if "S2_Tiles" in df.columns:
        pass
    else:
        df["S2_Tiles"] = df.S2Tiles

    # Compute latitude/longitude of field centroids
    r = []
    for row in df.itertuples():
        x, y = float(row.geometry.centroid.x), float(row.geometry.centroid.y)
        lon, lat = get_latlon_from_RD(x, y)
        r.append({"fieldid": row.Index, "latitude": lat, "longitude":lon, "rdx": x, "rdy": y})
    df_latlon = pd.DataFrame(r).set_index("fieldid")
    df = df.join(df_latlon)

    # add pixel counts
    for fname, col_name in [(counts_file_10m, "counts_10m"),
                            (counts_file_20m, "counts_20m"),
                            (counts_file_25m, "counts_25m")]:
        fname_counts = make_path_absolute(grompy_yaml, Path(fname))
        df_counts = pd.read_csv(fname_counts)
        df_counts.set_index("field_ID", inplace=True)
        df[col_name] = df_counts["count"]
        # Check for null values, e.g. fields without a count
        ix = df[col_name].isnull()
        if any(ix):
            print(f"{col_name}: found {sum(ix)} fields without a count: forcing zeros")
            df.loc[ix, col_name] = 0

    df_out = gpd.GeoDataFrame({"fieldID": df.index,
                               "year": df.year.astype(np.int32),
                               "counts_10m": df.counts_10m.astype(np.int32),
                               "counts_20m": df.counts_20m.astype(np.int32),
                               "counts_25m": df.counts_25m.astype(np.int32),
                               "area_ha": df.geometry.area / 1e4,
                               "rdx": df.rdx,
                               "rdy": df.rdy,
                               "longitude": df.longitude,
                               "latitude": df.latitude,
                               "cat_gewasc": df.cat_gewasc.apply(str),
                               "gws_gewasc": df.gws_gewasc.astype(np.int32),
                               "provincie": df.provincie.apply(str),
                               "gemeente": df.gemeente.apply(str),
                               "regio": df.regio.apply(str),
                               "pc4": df.PC4.apply(lambda pc4: str(int(pc4))),
                               "AHN2": df.AHN2,
                               "woonplaats": df.woonplaats.apply(str),
                               "waterschap": df.waterschap.apply(str),
                               "S2_Tiles": df.S2_Tiles.apply(str),
                               "geometry": df.geometry
                              }, crs=df.crs)
    df_out.reset_index(drop=True, inplace=True)

    df_out.to_file(gpkg_fname, layer=layer_name, driver="GPKG")
    field_ids = set(df_out.fieldID)
    df_out = df = df_counts = None

    return field_ids


def write_to_database(fname_duckdb, dataset_name, df):
    """routine writes data from a set of CSV files into an SQLite database using .import

    :param fname_duckdb: path to the database engine to be used
    :param dataset_name: the name of the dataset, will be used as output table name
    :param df: the dataframe to be written to the DB
    :return:
    """
    print(f"Start loading records to {fname_duckdb}, this can take some time...")
    with duckdb.connect(fname_duckdb) as DBconn:
        for i, df_chunk in enumerate(tqdm(np.array_split(df, 100))):
            DBconn.sql(f"INSERT INTO {dataset_name} SELECT * from df_chunk")

        DBconn.sql(f"CREATE UNIQUE INDEX {dataset_name}_uix ON {dataset_name} (fieldID, day)")
        cnt, = DBconn.sql(f"SELECT count(*) from {dataset_name}").fetchone()

    # check if all records are loaded
    if cnt == len(df):
        print(f"{cnt} records successfully loaded to db {fname_duckdb}")
    else:
        msg = f"Number of records loaded ({len(df)}) not equal to number of records written ({cnt}) to db ({fname_duckdb}). Check results!"
        print(msg)


def write_sensor_info(fname_duckdb, fname_sensor_info):
    """Write info about sensors in to the database provided by engine

    @param engine: the database engine
    @param sensor_info: the path to the CSV file containing sensor info

    The CSV files should look like::

        Date,Sensor,Overpass
        20210706,S2B,centraal
        20210709,S2B,west
        20210711,S2A,centraal
        20210717,S2A,zeeland
        20210721,S2A,centraal
        20210722,S2B,zeeland
        20210726,S2B,centraal

    and contain a record for all days within the grompy database.
    """
    df = pd.read_csv(fname_sensor_info)
    df = df.rename(columns=lambda x: x.lower())
    df["date"] = pd.to_datetime(df.date, format="%Y%m%d").dt.date
    with duckdb.connect(fname_duckdb) as DBconn:
        DBconn.sql(f"CREATE TABLE sensor_info AS SELECT * FROM df")


def process_df(df, value_name=None):
    """Process a CSV file with Sentinel2 parcel average observations from groenmonitor

    :param df: a pandas dataframe with inputs from groenmonitor
    :param value_name: the S2 band name to process
    :return:
    """
    df.drop(columns=["count"], inplace=True, errors="ignore")
    df1 = df.melt(id_vars="field_ID")
    df1["day"] = pd.to_datetime(df1.variable)
    df1.drop(columns=["variable"], inplace=True)
    df1.rename(columns={"value": value_name}, inplace=True)

    return df1.set_index(["field_ID","day"])


def start_serial_loading(grompy_yaml, datasets):
    """Start loading CSV files in sequence
    :param datasets:
    :return:
    """

    for dataset_name, description in datasets.items():
        if description is None:
            continue

        print(f"Starting loading of: {dataset_name}")
        df_all = None
        band_names = sorted(description["bands"].items(), key=lambda x: x[0])
        for column_name, csv_fname in tqdm(band_names):
            csv_fpath = make_path_absolute(grompy_yaml, Path(csv_fname))
            if df_all is None:
                df_all = process_df(pd.read_csv(csv_fpath), value_name=column_name)
            else:
                df_all = df_all.join(process_df(pd.read_csv(csv_fpath), value_name=column_name))

        # Remove empty rows
        df_all = df_all[df_all.values.sum(axis=1) != 0]
        df_all.reset_index(inplace=True)

        fname_duckdb = make_path_absolute(grompy_yaml, description["dsn"])
        prepare_db(fname_duckdb, table_name=dataset_name, bands=band_names)
        write_to_database(fname_duckdb, dataset_name, df_all)

        fname_ssinfo = make_path_absolute(grompy_yaml, description["sensor_info"])
        write_sensor_info(fname_duckdb, fname_ssinfo)


def load_data(grompy_yaml, parcel_info_only, parallel=False):
    """Loads data pointed to by the YAML config file.

    :param grompy_yaml: the path to grompy configuration file
    :param parcel_info_only: Only load parcel info, but no satellite data
    :param parallel: Load data using multiple CPUs, default False
    """

    grompy_conf = yaml.safe_load(open(grompy_yaml))

    # First load parcel info
    print("Start loading parcel information. This will take some time...")
    parcel_info = grompy_conf.pop("parcel_info")
    field_ids = load_parcel_info(grompy_yaml, **parcel_info)

    if not parcel_info_only:
        start_serial_loading(grompy_yaml, grompy_conf["datasets"])