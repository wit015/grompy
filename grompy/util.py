# -*- coding: utf-8 -*-
# Copyright (c) 2021 Wageningen Environmental Research
# Allard de Wit (allard.dewit@wur.nl), April 2021
from pathlib import Path
from packaging import version
import gzip

import yaml
import duckdb
from sqlalchemy import MetaData, Table, Column, Integer, Date, Float, Text, create_engine
from pyproj import Transformer
from . import __version__

def take_first(iter):
    for i in iter:
        return i


def count_lines(files):
    """Checks the number of lines in the input CSV files.
    """
    counts = {}
    print("Checking number of lines:")
    for fname in files:
        if fname.suffix == ".gz":
            with gzip.open(fname, 'rb') as myfile:
                for c, l in enumerate(myfile):
                    pass
                c += 1
        else:
            with open(fname) as my_file:
                c = sum(1 for _ in my_file)
        counts[fname] = c
        print(f" - {fname}: {c}")

    if len(set(counts.values())) > 1:
        msg = "WARNING: CSV files do not have the same number of " \
              "rows! Grompy will manage but it may indicate a problem in " \
              "the data."
        print(msg)
    else:
        print("OK: CSV files all have same length")

    return take_first(counts.values())


def create_ddb_table(table_name, band_names):

    sql = f"""CREATE TABLE {table_name} (
    fieldID INTEGER,
    day DATE,
    """
    for band, fname in band_names:
        if band == "NDVI":
            sql += f"{band} FLOAT, \n"
        else:
            sql += f"{band} USMALLINT, \n"

    sql += ")"
    return sql

def prepare_db(fname_ddb, table_name, bands):

    # Check if file exists, if so delete it
    fname_ddb.unlink(missing_ok=True)

    sql = create_ddb_table(table_name, bands)

    # Create new database and table structure
    with duckdb.connect(fname_ddb) as ddb:
        ddb.execute(sql)


def make_path_absolute(grompy_yaml, filepath):
    """Makes a path absolute, if a relative path is found it is asssumed to be relative
    to the location of grompy_conf

    :param grompy_yaml: the Path() of gromp_conf
    :param filepath: the (possible relative) Path of the input file

    :return: the absolute path to the file
    """
    filepath = Path(filepath)
    if not filepath.is_absolute():
        filepath = grompy_yaml.parent / filepath

    return filepath


def open_DB_connection(grompy_yaml, dsn):
    """This converts a DSN with a relative paths to an sqlite database to an absolute path.

    Other database DSNs are left untouched.
    """

    if dsn.startswith("sqlite"):
        sqlite_path = dsn.replace("sqlite:///", "")
        sqlite_path = make_path_absolute(grompy_yaml, Path(sqlite_path))
        dsn = f"sqlite:///{sqlite_path}"
    engine = create_engine(dsn)
    engine.connect()
    return engine


def get_latlon_from_RD(x, y, _cache={}):
    """Converts given X,Y in the Dutch RD coordinate system towards latitude longitude in WGS84

    For checking:
    in RD coordinate system X, Y: 197903, 309096
    should be in WGS84 latitude, longitude: 50.770195900951045, 5.995343676754713

    """
    rd_stelsel = "EPSG:7415"
    latlon_wgs84 = "EPSG:4326"
    try:
        rd2wgs84 = _cache["rd2wgs84"]
    except KeyError:
        rd2wgs84 = _cache["rd2wgs84"] = Transformer.from_crs(rd_stelsel, latlon_wgs84)

    return rd2wgs84.transform(x, y)


def check_grompy_version(grompy_yaml):
    grompy_conf = yaml.safe_load(open(grompy_yaml))
    v1 = version.parse(grompy_conf["grompy"]["version"])
    v2 = version.parse(__version__)
    if (v1.major, v1.minor) != (v1.major, v1.minor):
        msg = f"grompy.yaml ({v1}) does not match grompy version ({v2})! " \
              "Generate a new grompy.yaml file with `grompy init` " \
              "and adapt it."
        raise RuntimeError(msg)