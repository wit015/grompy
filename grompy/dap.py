# -*- coding: utf-8 -*-
# Copyright (c) 2021 Wageningen Environmental Research
# Allard de Wit (allard.dewit@wur.nl), April 2021
from pathlib import Path
from types import SimpleNamespace

import sqlalchemy as sa
import pandas as pd
import yaml
import duckdb

from .util import open_DB_connection, make_path_absolute
from .S2_zenith_azimuth_angles import df_angles


class DataAccessProvider:
    """grompy.DataAccessProvider allow to query grompy databases and iterate through the selected parcels.

    :param grompy_yaml: path to the grompy YAML file
    :param fieldID: A list of fieldIDs that should be selected. Note that the number of fieldIDs that can be
                    provided is limited but unknown (depending on the SQL parser). Above 1000 FieldIDs warning
                    messages will be displayed. When this keyword is used, all other selection criteria will
                    be ignored.
    :param limit: Limit the number of parcels selected.

    Various filters can be applied for selecting specific parcels or limiting the number of parcels returned:
    - Filtering on area:
        :keyword area_gt: Only selected parcels greater than X ha
        :keyword pixcount_10m_gt: Only select parcels with a 10 meter pixel count greater than this number
        :keyword pixcount_20m_gt: as for 20m
        :keyword pixcount_25m_gt: as for 25m
    - Filtering on crop/landcover type:
        :keyword gws_gewasc: Select only parcels with given gws_gewasc
        :keyword cat_gewasc: Select only parcels with given cat_gewasc
    - Filtering on region:
        :keyword provincie: Only select parcels within this province
        :keyword gemeente: Only select parcels within this gemeente
        :keyword postcode4: Only select parcels within this postcode4
        :keyword regio: Only select parcels within this regio
        :keyword woonplaats: Only select parcels within this woonplaats
        :keyword waterschap: Only select parcels within this waterschap
    - filtering on coordinate bounds:
        :keyword rdx_bounds: Select only parcels within the [min, max] RD X coordinates
        :keyword rdy_bounds: Select only parcels within the [min, max] RD Y coordinates
        :keyword lon_bounds: Select only parcels within the [min, max] longitude
        :keyword lat_bounds: Select only parcels within the [min, max] latitude
    """

    def __init__(self, grompy_yaml, fieldID=None, limit=None, **kwargs):

        grompy_yaml = Path(grompy_yaml).absolute()
        if not grompy_yaml.exists():
            msg = f"Cannot find config file: {grompy_yaml}"
            raise RuntimeError(msg)
        self.grompy_conf = yaml.safe_load(open(grompy_yaml))

        # Build connections to dataset tables
        self.dataset_connections = {}
        for dataset_name, description in self.grompy_conf["datasets"].items():
            if description is None:
                continue

            fname_duckdb = make_path_absolute(grompy_yaml, description["dsn"])
            DBconn = duckdb.connect(fname_duckdb, read_only=True)
            self.dataset_connections[dataset_name] = (DBconn, dataset_name)

            # For sentinel2 data also read the sensor information and generate the band names that need unit conversion
            if dataset_name == "sentinel2_reflectance_values":
                sensor_info = DBconn.sql("select * from sensor_info").df()
                sensor_info.set_index("overpass", inplace=True)
                self.observation_info = sensor_info.join(df_angles)
                self.observation_info.set_index(["date", "S2_tile"], inplace=True)
                self.bands2convert = [k for k in description["bands"].keys() if k.startswith("B")]

        self.datasets_enabled = set(self.dataset_connections.keys())

        #  Build connection to parcel info database and table
        self.parcel_dsn = "sqlite:///" + self.grompy_conf["parcel_info"]["gpkg_fname"]
        self.parcel_table = self.grompy_conf["parcel_info"]["layer_name"]
        self.engine = open_DB_connection(grompy_yaml, self.parcel_dsn)
        meta = sa.MetaData(self.engine)
        self.tbl_perc_info = sa.Table(self.parcel_table, meta, autoload=True)
        self.limit = int(1e9) if limit is None else limit

        # Build query for parcel selection, but avoid the geometry column "geo". SQLAlchemy wil
        # crash when selecting that one.
        selected_columns = [column for column in self.tbl_perc_info.c if column.name != "geom"]
        self.select_parcels = sa.select(selected_columns).order_by("fieldID")

        # Additional constraints to the parcel selection.
        if fieldID is not None:
            self._select_field_ids(fieldID)
        else:
            self._select_croplandcover_type(**kwargs)
            self._select_parcel_size(**kwargs)
            self._select_on_location(**kwargs)
            self._select_boundingbox(**kwargs)

        # Execute parcel selection query, but only to find number of parcels
        s = sa.select([sa.func.count()]).select_from(self.select_parcels)
        self.parcel_count = s.execute().fetchone()[0]

    def _select_field_ids(self, fieldID=None, **kwargs):
        """Select on specific field IDs, must be provide as a list, tuple or set.
        """
        if not isinstance(fieldID, (list, tuple, set)):
            msg = "FieldID must be provide as a list, set or tuple"
            raise RuntimeError(msg)
        if len(fieldID) > 1000:
            msg = f"Warning: your are passing {len(fieldID)} fieldIDs, this may be too many. " \
                  f"Consider splitting your selected fieldIDs in smaller batches or use another " \
                  f"selection criteria."
        self.select_parcels.append_whereclause(self.tbl_perc_info.c.fieldID.in_(fieldID))

    def _select_croplandcover_type(self, gws_gewasc=None, cat_gewasc=None, **kwargs):
        """select on crop/land cover type
        """
        if gws_gewasc is not None:
            self.select_parcels.append_whereclause(self.tbl_perc_info.c.gws_gewasc == gws_gewasc)
        if cat_gewasc is not None:
            cat_gewasc = str(cat_gewasc).strip()
            self.select_parcels.append_whereclause(self.tbl_perc_info.c.cat_gewasc == cat_gewasc)

    def _select_parcel_size(self, area_gt=None, pixcount_10m_gt=None, pixcount_20m_gt=None, pixcount_25m_gt=None, **kwargs):
        """Select on field size and pixel counts
        """
        if area_gt is not None:
            self.select_parcels.append_whereclause(self.tbl_perc_info.c.area_ha > area_gt)
        if pixcount_10m_gt is not None:
            self.select_parcels.append_whereclause(self.tbl_perc_info.c.counts_10m > pixcount_10m_gt)
        if pixcount_20m_gt is not None:
            self.select_parcels.append_whereclause(self.tbl_perc_info.c.counts_10m > pixcount_20m_gt)
        if pixcount_25m_gt is not None:
            self.select_parcels.append_whereclause(self.tbl_perc_info.c.counts_10m > pixcount_25m_gt)

    def _select_on_location(self, provincie=None, gemeente=None, postcode4=None, regio=None,
                            woonplaats=None, waterschap=None, **kwargs):
        """Select on provinces, postal codes, etc.
        """
        if provincie is not None:
            self.select_parcels.append_whereclause(self.tbl_perc_info.c.provincie == provincie)
        if gemeente is not None:
            self.select_parcels.append_whereclause(self.tbl_perc_info.c.gemeente == gemeente)
        if postcode4 is not None:
            self.select_parcels.append_whereclause(self.tbl_perc_info.c.pc4 == postcode4)
        if regio is not None:
            self.select_parcels.append_whereclause(self.tbl_perc_info.c.regio == regio)
        if woonplaats is not None:
            self.select_parcels.append_whereclause(self.tbl_perc_info.c.woonplaats == woonplaats)
        if waterschap is not None:
            self.select_parcels.append_whereclause(self.tbl_perc_info.c.waterschap == waterschap)

    def _select_boundingbox(self, rdx_bounds=None, rdy_bounds=None, lon_bounds=None, lat_bounds=None, **kwargs):
        """Select on coordinates, either geographic or lat/lon.
        """
        if rdx_bounds is not None:
            self.select_parcels.append_whereclause(sa.between(self.tbl_perc_info.c.rdx, rdx_bounds[0], rdx_bounds[1]))
        if rdy_bounds is not None:
            self.select_parcels.append_whereclause(sa.between(self.tbl_perc_info.c.rdy, rdy_bounds[0], rdy_bounds[1]))
        if lon_bounds is not None:
            self.select_parcels.append_whereclause(sa.between(self.tbl_perc_info.c.longitude, lon_bounds[0], lon_bounds[1]))
        if lat_bounds is not None:
            self.select_parcels.append_whereclause(sa.between(self.tbl_perc_info.c.latitude, lat_bounds[0], lat_bounds[1]))


    @property
    def datasets(self):
        """Return available datasets specified in grompy.yaml"""
        return list(self.dataset_connections.keys())

    def enable(self, dataset):
        """Enables a dataset for reading with grompy..
        """
        if dataset in self.datasets:
            self.datasets_enabled.add(dataset)
        else:
            print(f"'{dataset}' unknown, should be one of: {self.datasets}")

    def disable(self, dataset):
        """Disable a dataset for reading with grompy.
        """
        if dataset in self.datasets:
            if dataset in self.datasets_enabled:
                self.datasets_enabled.remove(dataset)
        else:
            print(f"'{dataset}' unknown, should be one of: {self.datasets}")

    def _add_S2_angles(self, df, parcel_info):
        """Adds the Sentinel2 view azimuth/zenith angles to the grompy dataframe.

        :param df: the grompy dataframe with sentinel2 reflectance values
        :type df: pandas.DataFrame
        :param parcel_info: A namedtuple with parcel information
        :type parcel_info: NamedTuple
        :return: a grompy DataFrame with S2 viewing azimuth/zenith angles added.
        """
        if hasattr(parcel_info, "S2tiles"):
            S2tile = parcel_info.S2tiles.split(",")[0]
        elif hasattr(parcel_info, "S2_tiles"):
            S2tile = parcel_info.S2_tiles.split(",")[0]
        else:
            msg = "Parcel info does not have attribute 'S2tiles' or 'S2_tiles'"
            raise RuntimeError(msg)
        df["S2_tile"] = S2tile
        df["date"] = pd.to_datetime(df.day)
        df.set_index(["date", "S2_tile"], inplace=True)
        df = df.join(self.observation_info, how="inner")
        df.reset_index(inplace=True, drop=True)

        return df

    def __iter__(self):
        """Iterates over the selected parcels and returns a tuple of (parcel_info, observations) for each
        iteration.
        """

        r = self.select_parcels.limit(self.limit).execute()
        rows = r.fetchmany(100)
        while rows:
            for parcel_info in rows:
                d = dict()
                for dataset_name, (DBconn, tbl) in self.dataset_connections.items():
                    if dataset_name not in self.datasets_enabled:
                        continue
                    sql = f"SELECT * EXCLUDE (fieldID) FROM {tbl} WHERE fieldID=? ORDER BY day"
                    df = DBconn.execute(sql, [parcel_info.fieldID]).df()
                    if dataset_name == "sentinel2_reflectance_values":
                        df = self._add_S2_angles(df, parcel_info)
                    df.index = pd.to_datetime(df.day)
                    df.drop(columns="day", inplace=True)
                    if dataset_name.startswith("sentinel2"):
                        df[self.bands2convert] = df[self.bands2convert].astype(float) / 10000.0
                    d[dataset_name] =  df
                c = SimpleNamespace(**d)

                yield parcel_info, c
            rows = r.fetchmany(100)

    def __len__(self):
        return self.parcel_count
