# -*- coding: utf-8 -*-
# Copyright (c) 2021 Wageningen Environmental Research
# Allard de Wit (allard.dewit@wur.nl), October 2021
"""Grompy is a tool to process and access parcel-based satellite observations from GroenMonitor.nl.
"""
__version__ = "1.6.1"

from .dap import DataAccessProvider
from .cmd import cli


